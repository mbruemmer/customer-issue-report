#!/usr/bin/env python3

import gitlab
import json
import csv
import os
import argparse
import re
import urllib.parse
import time
from mako.template import Template

def read_customer_file(filename):
    '''read a csv file containing customers and their CS projects,
    as well as the issue containing links to issues they observe'''

    customers = {}
    try:
        with open(filename,"r") as customer_project_file:
            csvReader = csv.DictReader(customer_project_file, delimiter="\t")
            for row in csvReader:
                customers[row["customer_id"]] = dict(row)
        return customers
    except:
        print("Could not open file:%s" % filename)
        exit(0)

def read_report(report_file_path):
    '''read a json report as produced by this script.'''
    try:
        with open(report_file_path, "r") as report_file:
            previous_report = json.load(report_file)
            return previous_report
    except json.decoder.JSONDecodeError:
        print("Report not valid:%s" % report_file_path)
        return None
    except:
        print("No previous report found:%s" % report_file_path)
        return None

def parse_description(gl, issue):
    issue_regex = gl.url + ".*/issues/[0-9]+"

    issue_iter = re.finditer(issue_regex,issue.description)
    parsed_issues = set()
    for issue_match in issue_iter:
        parsed_issues.add(issue_match.group())
        # can use issue_match.span() to get string position and search backwards for priority labels
    issues_objects = []
    for issue in parsed_issues:
        project_id = issue[len(gl.url):issue.rfind("issues")-1]
        project_id = urllib.parse.quote(project_id, safe='')
        issue_id = issue[issue.rfind("/")+1:]
        project = gl.projects.get(project_id, lazy=True)
        try:
            issue_object = project.issues.get(issue_id)
        except:
            print("Issue not found: %s" % issue)
            continue
        time.sleep(1)
        issues_objects.append(issue_object)
    return issues_objects


def compare_issues(old_issue, new_issue):
    '''compare an issue with its previous state to indicate which fields were updated'''
    #could also track stale issues by setting a cutoff date (2 releases?)
    if old_issue["update_date"] != new_issue["update_date"]:
        new_issue["issue_updated"] = True
    else:
        new_issue["issue_updated"] = False
        
    if old_issue["title"] != new_issue["title"]:
        new_issue["title_updated"] = True
    else:
        new_issue["title_updated"] = False
        
    if old_issue["milestone"] != new_issue["milestone"]:
        new_issue["previous_milestone"] = old_issue["milestone"]
        new_issue["milestone_updated"] = True
    else:
        new_issue["milestone_updated"] = False
        
    if old_issue["state"] != new_issue["state"]:
        new_issue["state_updated"] = True
    else:
        new_issue["state_updated"] = False

    new_issue["added_labels"] = list(set(new_issue["labels"]) - set(old_issue["labels"]))
    new_issue["removed_labels"] = list(set(old_issue["labels"]) - set(new_issue["labels"]))
    new_issue["unchanged_labels"] = list(set(new_issue["labels"]).intersection(set(old_issue["labels"])))
    return new_issue

def annotate_changes(current_report, previous_report):
    '''add changed fields for all issues between two versions of a report'''
    for previous_customer_id, previous_customer_data in previous_report.items():
        #if known customer, else no changes
        if previous_customer_id in current_report:
            current_customer_data = current_report[previous_customer_id]
            if "issues" in previous_customer_data:
                for previous_issue_id, previous_issue_data in previous_customer_data["issues"].items():
                    #found a match, compare issues
                    if str(previous_issue_id) in current_customer_data["issues"]:
                        current_issue = current_customer_data["issues"][str(previous_issue_id)]
                        current_issue["recently_added"] = False
                        current_issue = compare_issues(previous_issue_data, current_issue)
                previous_customer_data = previous_report[previous_customer_id]
                for current_issue_id, current_issue_data in current_customer_data["issues"].items():
                    #tickets we have not seen before
                    if str(current_issue_id) not in previous_customer_data["issues"]:
                        current_issue = current_customer_data["issues"][str(current_issue_id)]
                        current_issue["recently_added"] = True
                        # gotta add all the "changes fields"
                        current_issue = compare_issues(current_issue, current_issue)
            else:
                print("No issues found.")
                return None


def write_csv(project):
    file_name = project["customer_id"] + "_" + project["customer_cs_project"].replace("/","_") + "_" + project["cs_issues_issue_id"] + ".csv"

    fields = ["id","title","url","update_date","state","milestone","labels"]

    with open(file_name,"w") as issue_file:
        reportwriter = csv.writer(issue_file, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        reportwriter.writerow(fields)
        if "issues" in project:
            for issue in project["issues"].values():
                row = []
                for field in fields:
                    if field == "labels":
                        row.append(", ".join(issue[field]))
                    else:
                        row.append(issue[field])
                reportwriter.writerow(row)

def request_issues(gl, customer):
    customer_project_id = urllib.parse.quote(customer["customer_cs_project"], safe='')
    print(customer_project_id)
    try:
        cs_project = gl.projects.get(customer_project_id)
    except:
        print("Could not retrieve %s" % customer["customer_cs_project"])
        return None

    issue_list_issue = cs_project.issues.get(customer["cs_issues_issue_id"])
    issues_to_observe = issue_list_issue.links.list()

    #parse description to find issue links in description
    issues_in_text = parse_description(gl, issue_list_issue)
    if issues_in_text:
        issues_to_observe.extend(issues_in_text)

    customer_issues = {}
    for issue in issues_to_observe:
        cs_issue = {}
        cs_issue["title"] = issue.title
        cs_issue["url"] = issue.web_url
        cs_issue["update_date"] = issue.updated_at
        cs_issue["state"] = issue.state
        cs_issue["id"] = issue.id
        if issue.milestone is not None:
            cs_issue["milestone"] = issue.milestone["title"]
        else:
            cs_issue["milestone"] = None
        cs_issue["labels"] = issue.labels
        customer_issues[str(issue.id)] = cs_issue
    customer["issues"] = customer_issues
    return customer

parser = argparse.ArgumentParser(description='Create report for GitLab issues')
parser.add_argument('-g','--gitlaburl', default="https://gitlab.com/", help='url of the gitlab instance holding the project')
parser.add_argument('token', help='API token able to read the requested projects')
parser.add_argument('projectfile', help='CSV file that defines requested projects')
args = parser.parse_args()

gitlaburl = args.gitlaburl if args.gitlaburl.endswith("/") else args.gitlaburl + "/"
gl = gitlab.Gitlab(gitlaburl, private_token=args.token)

customers_report = read_customer_file(args.projectfile)

for customer in customers_report.values():
    customer = request_issues(gl, customer)

previous_report = read_report("issue_report.json")

annotate_changes(customers_report, customers_report)
if previous_report:
    annotate_changes(customers_report, previous_report)

with open("issue_report.json","w") as report_file:
    json.dump(customers_report, report_file, indent=4)

# produce csvs

for project in customers_report.values():
    write_csv(project)

mytemplate = Template(filename='template/index.html')
label_colors = {"Accepting merge requests":"badge-success", "missed":"badge-danger"}
with open("public/index.html","w") as outfile:
    try:
        outfile.write(mytemplate.render(report = customers_report, gitlab = gitlaburl, label_colors = label_colors))
    except:
        print("Could not render HTML")
# Customer issues report

Create report of observed issues using the GitLab API and GitLab pages.
Observed issues are retrieved from "related issues" in a customer issue like 
[here](https://gitlab.com/mbruemmer/customer-issue-report/issues/1).
Highlight differences since pipeline was last run.

Example page: https://mbruemmer.gitlab.io/customer-issue-report

## Features

- Queries all issues related to the ones specified in `customerProjects.csv`
- Retrieves the previous report artifact in JSON and highlights changes
  - Highlighted changes include changes to issues like changed state, milestone or labels
- Produces HTML, JSON and CSV
- Parses issues linked in the description of the issue as well

## Configuration

- Export / fork repository.
- Add a GitLab API token to CI/CD variables named `GIT_TOKEN` and set it masked.
This token will be used to query the API for issues and projects.
It is also used to download the last successful report artifact for comparison.
You can use different tokens for API calls and artifact download by modifying `.gitlab-ci.yml`
- Modify `customerProjects.csv` and add one line for each project 
as well as the id of the issue holding related issues to list. `customer_id` should be unique,
`link` can be anything but salesforce is intended.
- Make sure Settings -> Permissions -> Pages is limited to project members
- Run the Pipeline to get your report
